// Package utils contains miscellaneous functions
package utils

import (
	"os"
	"users-microservice/constants"

	log "github.com/sirupsen/logrus"
)

// GetCloudEventSourceString gets the pod name from the environment variable; otherwise, just reports an error and returns an empty string
func GetCloudEventSourceString() string {
	log.Trace("utils.GetCloudEventSourceString() start")
	pname := os.Getenv(constants.EnvPodNameVar)
	if pname == "" {
		log.Warn("?Could not obtain the pod name")
	}
	return constants.CloudEventSourceBaseValue + pname
}

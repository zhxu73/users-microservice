package adapters

import (
	"context"
	"encoding/json"
	"regexp"
	"sync"
	"users-microservice/constants"
	"users-microservice/types"

	"github.com/nats-io/stan.go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging"
	"gitlab.com/cyverse/cacao-common/service"
)

// EventAdapter adapter to the IncomingEventsPort and the OutgoingEventsPort
type EventAdapter struct {
	natsURL           string
	stanQgroup        string
	stanDurableName   string
	stanClusterID     string
	stanClientID      string
	domainUserChannel chan types.UserOp
}

// Init connects to the events queue and subscribe to it
func (ea *EventAdapter) Init(s constants.Specification) error {
	log.Trace("EventAdapter.Init()")

	// only initialized, if it hasn't already been done
	if ea.natsURL == "" {
		ea.natsURL = s.NatsURL
		ea.stanQgroup = constants.DefaultNatsQGroup
		ea.stanDurableName = constants.DefaultNatsDurableName
		ea.stanClusterID = s.ClusterID
		ea.stanClientID = constants.ClientID // TODO: change this to be configurable
	}

	return nil
}

// InitChannel initializes the channel with the domain object
func (ea *EventAdapter) InitChannel(uc chan types.UserOp) {
	log.Trace("EventAdapter.InitChannel()")
	ea.domainUserChannel = uc
}

// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (ea *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup) {
	log.Trace("EventAdapter.Start()")

	defer wg.Done()

	log.Trace("EventAdapter.Start: stan connect for request")
	sc, err := stan.Connect(ea.stanClusterID, ea.stanClientID, stan.NatsURL(ea.natsURL))
	if err != nil {
		log.Fatal("Cannot connect to nats, cluster id = "+ea.stanClusterID+", client id="+
			ea.stanClientID+"nats url = "+ea.natsURL+": ", err)
	}
	defer sc.Close()

	// Subscribe
	log.Trace("EventAdapter.Start: subscribing on events queue '" + constants.DefaultNatsEventsSubject + "'")
	sub, err := sc.QueueSubscribe(constants.DefaultNatsEventsSubject, ea.stanQgroup,
		func(m *stan.Msg) {
			log.Trace("EventAdapter.Start: within stan function (" + ea.stanClientID + ")")

			m.Ack()

			ce, err := messaging.ConvertStan(m)
			if err != nil {
				log.Debug("EventAdapter.Start: could not convert, skipping (" + ea.stanClientID + ")")
				return
			}

			log.Debug("EventAdapter.Start: converted cloudevent, type = " + ce.Type() + " (" + ea.stanClientID + ")")

			// setup and unmarshal the User query object in question
			userop := types.UserOp{}
			queryUser := service.UserModel{}
			err = json.Unmarshal(ce.Data(), &queryUser)
			if err != nil {
				log.Debug("EventAdapter.Start: unmarshaling User object from cloudevent: " + err.Error())
				return
			}

			match, _ := regexp.MatchString("org.cyverse.events.User(Add|Update|Delete)Requested", ce.Type())
			if !match {
				log.Trace("EventAdapter.Start: received event " + ce.Type() + ", skipping.")
				return
			}
			log.Debug("EventAdapter.Start: received event " + ce.Type())

			userop.User = types.NewDomainUserModel(queryUser)
			userop.Op = ce.Type()

			log.Trace("EventAdapter.Start: sending data to domain channel for processing")
			ea.domainUserChannel <- userop

		}, stan.DurableName(ea.stanDurableName))
	if err != nil {
		log.Fatal("?QueryAdapter.Start: Cannot create a queued sync subscription" + err.Error())
	}
	defer sub.Unsubscribe()

	log.Trace("EventAdapter.Start: waiting for being done")
	<-ctx.Done()

	log.Trace("EventAdapter.Start: no longer waiting for events")
}

// PublishEvent publishes an an event to the event queue
// TODO: deal with the situation when an event cannot be published; one approach
// would be for the domain object to store unpublished events in the persistent
// store and then publish when the queue is available again
func (ea *EventAdapter) PublishEvent(ev common.EventType, duser types.DomainUserModel) error {
	log.Trace("EventAdapter.PublishEvent() start with eventType = " + string(ev))

	suser := duser.ConvertToServiceUserModel(duser.SessionActor, duser.SessionEmulator)

	log.Trace("EventAdapter.PublishEvent: stan connect for request")
	sc, err := stan.Connect(ea.stanClusterID, ea.stanClientID+constants.ClientIDResponderExt, stan.NatsURL(ea.natsURL))
	if err != nil {
		return err
	}
	defer sc.Close()

	log.Trace("EventAdapter.PublishEvent: stan connect for request")
	ce, err := messaging.CreateCloudEvent(suser, string(ev), ea.stanClientID)
	if err != nil {
		return err
	}
	payload, err := json.Marshal(ce)
	if err != nil {
		return err
	}

	log.Trace("EventAdapter.PublishEvent: sync publishing the event " + ev)
	err = sc.Publish(constants.DefaultNatsEventsSubject, payload)
	if err != nil {
		return err
	}

	return nil
}

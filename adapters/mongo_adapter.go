package adapters

import (
	"context"
	"errors"
	"strconv"
	"time"
	"users-microservice/constants"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"

	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoAdapter is the adapter implementation to PersistentStoragePort but for Mongo
type MongoAdapter struct {
	connection *db.MongoDBConnection
	collection *mongo.Collection
	ctx        context.Context
}

// Init - establish the connection to Mongodb and create the collection
func (m *MongoAdapter) Init(s constants.Specification) error {

	mconfig := db.MongoDBConfig{
		URL:    s.DbURI,
		DBName: s.DbName}

	var err error
	m.connection, err = db.NewMongoDBConnection(&mconfig)
	if err != nil {
		return err
	}

	err = m.connection.Client.Ping(s.AppContext, nil)
	if err != nil {
		return err
	}

	m.collection = m.connection.Client.Database(s.DbName).Collection(constants.MongoDbUserCollectionName)
	m.ctx = s.AppContext
	return nil
}

// UserAdd - add a new user to the database
// This method will automatically update the CreatedAt and UpdatedAt
func (m *MongoAdapter) UserAdd(user *types.DomainUserModel) error {
	log.Trace("MongoAdapter.UserAdd() start, username = " + user.Username)

	// let's add values to the created field names
	user.CreatedAt = time.Now()
	user.UpdatedAt = user.CreatedAt
	user.UpdatedBy = user.SessionActor

	_, err := m.collection.InsertOne(m.ctx, user)
	if err != nil {
		return err
	}

	return nil
}

// UserDelete - delete a user from the database
func (m *MongoAdapter) UserDelete(username string) error {
	log.Trace("MongoAdapter.UserDelete() start, username = " + username)

	filter := bson.M{"_id": username}
	resp, err := m.collection.DeleteOne(m.ctx, filter)
	if err != nil {
		return err
	} else if resp.DeletedCount == 0 {
		return errors.New(service.UserUsernameNotFoundError)
	}
	return nil
}

// UserGet - return a user and associated data from the database
func (m *MongoAdapter) UserGet(user *types.DomainUserModel) error {
	log.Trace("MongoAdapter.UserGet() start, username = " + user.Username)
	filter := bson.M{"_id": user.Username}
	r := m.collection.FindOne(m.ctx, filter)
	if r.Err() == mongo.ErrNoDocuments {
		log.Trace("MongoAdapter.UserGet:\tnot found")
		return errors.New(service.UserUsernameNotFoundError)
	}
	if err := r.Decode(user); err != nil {
		log.Trace("MongoAdapter.UserGet:\tcould not decode user")
		return err
	}
	return nil
}

// UserList returns a list of the users in the database
func (m *MongoAdapter) UserList(filter service.UserListFilter) ([]types.DomainUserModel, error) {
	log.Trace("MongoAdapter.UsersList() start, " + string(filter.Field) + "=" + filter.Value + ", sort=" +
		strconv.Itoa(int(filter.SortBy)) + ", start=" + strconv.Itoa(filter.Start) + ", maxItems=" + strconv.Itoa(filter.MaxItems))

	filterField := bson.M{} // default is to return all
	switch filter.Field {
	case service.IsAdminFilterField:
		boolval, _ := strconv.ParseBool(filter.Value)
		filterField = bson.M{"is_admin": boolval}
	default:

	}

	// default sort is by
	log.Trace("MongoAdapter.UsersList: constructing options")
	opts := options.Find()
	if filter.SortBy == service.AscendingSort {
		opts.SetSort(bson.D{primitive.E{Key: "_id", Value: 1}})
	} else {
		opts.SetSort(bson.D{primitive.E{Key: "_id", Value: -1}})
	}
	opts.SetLimit(int64(filter.MaxItems))
	opts.SetSkip(int64(filter.Start))

	log.Trace("MongoAdapter.UsersList: executing find")
	cur, err := m.collection.Find(m.ctx, filterField, opts)
	if err != nil {
		return nil, err
	}
	defer cur.Close(m.ctx)

	var dusers []types.DomainUserModel
	err = cur.All(m.ctx, &dusers)
	if err != nil {
		return nil, err
	}

	return dusers, nil
}

// UserUpdate - update the properties for a user in the database
// This method will automatically update the UpdatedAt
func (m *MongoAdapter) UserUpdate(user *types.DomainUserModel) error {
	log.Trace("MongoAdapter.UserUpdate() start, username = " + user.Username)

	// opts := options.Update().SetUpsert(false)
	filter := bson.M{"_id": user.Username}

	user.UpdatedAt = time.Now()
	user.UpdatedBy = user.SessionActor

	_, err := m.collection.ReplaceOne(m.ctx, filter, user)
	if err != nil {
		return err
	}
	return nil
}

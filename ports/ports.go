package ports

import (
	"context"
	"sync"
	"users-microservice/constants"
	"users-microservice/types"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// Port is the base interface for Ports, in which all should support an Init() function (but it may be noop)
type Port interface {
	Init(s constants.Specification) error
}

// AsyncPort is a special type of port that will has an asynchronous behavior to it, such as being event-driven
// outside of the primary domain thread (e.g. incoming message queues). This ports should communicate with the
// domain object using a threaded approach (i.e. go routine, go channels and possibly waitgroups)
type AsyncPort interface {
	Port
	Start(context.Context, *sync.WaitGroup)
}

// IncomingQueryPort is an example interface for a query port.
// Internal to the adapter there may be additional methods, but at the very least, it is an async port
// Also, incoming and outgoing ports are declared separately for illustrative purposes
// method set signatures will allow one adapter fullfill the purposes of both income and outoing query ports
type IncomingQueryPort interface {
	AsyncPort
	// TODO InitChannel(u chan service.User, ulist chan []service.IUserList)
	InitChannel(uc chan types.UserOp, ul chan types.UserListQuery)
}

// IncomingEventsPort is an example interface for an event port.
type IncomingEventsPort interface {
	AsyncPort
	InitChannel(uc chan types.UserOp)
}

// OutgoingEventsPort is the port for events
type OutgoingEventsPort interface {
	Port
	PublishEvent(ev common.EventType, duser types.DomainUserModel) error
}

// PersistentStoragePort is a port to store messages
type PersistentStoragePort interface {
	Port
	UserAdd(*types.DomainUserModel) error
	UserDelete(string) error
	UserGet(*types.DomainUserModel) error
	UserUpdate(*types.DomainUserModel) error
	UserList(service.UserListFilter) ([]types.DomainUserModel, error)
}

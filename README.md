# Users Microservice
A microservice for managing users and user preferences for use within CACAO and other related services. The architecture of this microservice uses the ports/adapters or hexagonal architecture.

# Usage

You can run the users microservice via command line, simply by running the command:

```go run main.go```

You can configure the microservice using environment variables

| Environment Variable | Description | Default |
|--- |--- |--- |
| USERSMS_DB_URI | mongo db uri | mongodb://localhost:27017 |
| USERSMS_DB_NAME | mongo database name | users |
| USERSMS_CLUSTER_ID | nats streaming cluster id | cacao-cluster |
| USERSMS_NATS_URL | nats url | nats://nats:4222 |
| USERSMS_LOG_LEVEL | debug level, either one of "debug", "trace", "info" | debug | 

# Directory Layout
main.go - main executable for the users microservice

adapters/ - contains adapters, or the implementations to ports

constants/ - contains constants or defaults

domain/ - contains the central domain object

types/ - contains common types used across the users microservice
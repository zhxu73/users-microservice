package domain

import (
	"context"
	"sync"

	"users-microservice/constants"
	"users-microservice/ports"
	"users-microservice/types"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/service"
)

// Domain is the base struct for the domain service
type Domain struct {
	QueryIn ports.IncomingQueryPort
	//QueryOut     ports.OutgoingQueryPort
	EventsIn     ports.IncomingEventsPort
	EventsOut    ports.OutgoingEventsPort
	UserStore    ports.PersistentStoragePort
	userHandlers map[string]UserHandleFunc
	listHandlers map[string]UserListHandleFunc
}

// UserHandleFunc is the function handler for both queries and events
type UserHandleFunc func(ctx context.Context, query *types.UserOp, bypassSessionCheck bool)

// UserListHandleFunc is the function handler for list queries
type UserListHandleFunc func(ctx context.Context, query *types.UserListQuery, bypassSessionCheck bool)

// NewDomain returns a domain object given the required parameters
func NewDomain(qin ports.IncomingQueryPort, ein ports.IncomingEventsPort, eout ports.OutgoingEventsPort, store ports.PersistentStoragePort) *Domain {
	return &Domain{
		QueryIn:   qin,
		EventsIn:  ein,
		EventsOut: eout,
		UserStore: store,
	}
}

// Init initializes all the specified adapters
func (d *Domain) Init(c constants.Specification) error {
	log.Debug("domain.Init() starting")

	err := d.EventsIn.Init(c)
	if err != nil {
		return err
	}

	err = d.QueryIn.Init(c)
	if err != nil {
		return err
	}

	err = d.UserStore.Init(c)
	if err != nil {
		return err
	}

	// Let's setup the handlers here
	// TODO: remove the service from the domain object
	d.userHandlers = make(map[string]UserHandleFunc)
	d.userHandlers[service.NatsSubjectUsersGet] = d.readUserHandler
	d.userHandlers[string(service.EventUserAddRequested)] = d.addUserHandler
	d.userHandlers[string(service.EventUserUpdateRequested)] = d.updateUserHandler
	d.userHandlers[string(service.EventUserDeleteRequested)] = d.deleteUserHandler

	d.listHandlers = make(map[string]UserListHandleFunc)
	d.listHandlers[service.NatsSubjectUsersList] = d.readUserListHandler
	return nil
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) {
	log.Debug("domain.Start() starting")

	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	// It is here that we need to handle asynchronous adapters
	// create channel for incoming queries, qchan
	querychan := make(chan types.UserOp, constants.DefaultChannelBufferSize)
	listchan := make(chan types.UserListQuery, constants.DefaultChannelBufferSize)
	d.QueryIn.InitChannel(querychan, listchan)
	wg.Add(1)
	go d.QueryIn.Start(ctx, &wg)

	// // start the domain's query worker
	wg.Add(1)
	go d.processQueryWorker(ctx, querychan, listchan, &wg)

	// create a channel for events, echan
	echan := make(chan types.UserOp, constants.DefaultChannelBufferSize)
	d.EventsIn.InitChannel(echan)
	wg.Add(1)
	go d.EventsIn.Start(ctx, &wg)

	// start the domain's query worker
	wg.Add(1)
	go d.processEventWorker(ctx, echan, &wg)

	wg.Wait()
}

func (d *Domain) processQueryWorker(ctx context.Context, userchan chan types.UserOp, listchan chan types.UserListQuery, wg *sync.WaitGroup) {
	log.Debug("domain.processQueryWorker starting")
	defer wg.Done()

	for {
		select {
		case data := <-userchan:
			log.Trace("domain.processWorker received new data")
			log.Trace(data)
			f, found := d.userHandlers[data.Op]
			if found {
				f(ctx, &data, false)
			} else {
				log.Warn("domain query handler was not found, op = " + data.Op)
			}
		case data := <-listchan:
			log.Trace("domain.processWorker received new data")
			log.Trace(data)
			f, found := d.listHandlers[data.Op]
			if found {
				f(ctx, &data, false)
			} else {
				log.Warn("domain query list handler was not found, op = " + data.Op)
			}
		case <-ctx.Done():
			log.Trace("\tdomain.processWorker context cancellation")
		}
	}
}

func (d *Domain) processEventWorker(ctx context.Context, echan chan types.UserOp, wg *sync.WaitGroup) {
	log.Debug("domain.processEventWorker starting")
	defer wg.Done()

	for {
		select {
		case data := <-echan:
			log.Trace("domain.processEventWorker received new data")
			log.Trace(data)
			f, found := d.userHandlers[data.Op]
			if found {
				f(ctx, &data, false)
			} else {
				log.Warn("domain event list handler was not found, op = " + data.Op)
			}

		case <-ctx.Done():
			log.Trace("\tdomain.processEventWorker context cancellation")
		}
	}
}

// getActorAuthorization simply checks the actor authorization based on the operation
// this has the side effect of updating the error messages in the user object
// userContext is the username in question, for a list just set to ""
// TODO: improve return, currently errorType, errorMessage, boolean whether authorized or not, and boolean isAdmin
func (d *Domain) getActorAuthorization(op string, actor string, userContext string) (string, string, bool, bool) {
	log.Trace("domain.getActorAuthorization: starting. Actor = " + actor + ", op = " + op)

	authorized := true
	errorType := ""
	errorMessage := ""
	isAdmin := false

	// check if the actor is a special system one
	if actor == service.ReservedCacaoSystemActor {
		log.Trace("domain.getActorAuthorization:\tservice.ReservedCacaoSystemActor authorized")
		isAdmin = true
		return errorType, errorMessage, authorized, isAdmin
	}

	// next get the actor as a user
	actorUser := types.DomainUserModel{Username: actor}
	err := d.UserStore.UserGet(&actorUser)
	if err != nil { // there was an error
		log.Trace("domain.getActorAuthorization: error = " + service.GeneralActorNotFoundError)
		errorType = service.GeneralActorNotFoundError
		errorMessage = "User '" + actor + "' as an actor is not foud '"
		authorized = false
		return errorType, errorMessage, authorized, isAdmin
	} else if actorUser.IsAdmin { // at this point, the user is an admin, can do anything
		isAdmin = true
		return errorType, errorMessage, authorized, isAdmin
	}

	switch op {
	case string(service.EventUserDeleteRequested), service.NatsSubjectUsersList: // admin only
		if !actorUser.IsAdmin {
			log.Trace("domain.getActorAuthorization: error = " + service.GeneralActorNotAuthorizedError)

			authorized = false
			errorType = service.GeneralActorNotAuthorizedError
			errorMessage = "User '" + actor + "' is not authorized to get user info about '" +
				userContext + "'"
			return errorType, errorMessage, authorized, isAdmin
		}
	case service.NatsSubjectUsersGet: // this would be equivalent
		fallthrough // for now, let's assume only admins can get their own
	default:
		if userContext != actor {
			actorUser := types.DomainUserModel{Username: actor}
			err := d.UserStore.UserGet(&actorUser)
			if err != nil {
				log.Trace("domain.getActorAuthorization: error = " + service.GeneralActorNotFoundError)
				errorType = service.GeneralActorNotFoundError
				errorMessage = "User '" + actor + "' as an actor is not foud '"
				authorized = false
			} else if !actorUser.IsAdmin { // at this point, this is a user who is not an admin wanted to get another user -> unauthorized
				log.Trace("domain.getActorAuthorization: error = " + service.GeneralActorNotAuthorizedError)

				authorized = false
				errorType = service.GeneralActorNotAuthorizedError
				errorMessage = "User '" + actor + "' is not authorized to get user info about '" +
					userContext + "'"
			}
		}
	}

	return errorType, errorMessage, authorized, isAdmin
}

// readUserHandler will get the user from the UserStore.
// Authorization: user can currently only read their own information, unless isAdmin
// bypassSessionCheck: if true, it will not check authorization of the user
func (d *Domain) readUserHandler(ctx context.Context, query *types.UserOp, bypassSessionCheck bool) {
	log.Trace("domain.readUserHandler: starting. looking for " + query.User.Username)

	// Users can always get their own info; otherwise, must be admin
	var authorized bool
	query.User.ErrorType, query.User.ErrorMessage, authorized, _ = d.getActorAuthorization(query.Op, query.User.SessionActor, query.User.Username)
	if authorized {
		err := d.UserStore.UserGet(query.User)
		if err != nil { // for now, assuming not found
			log.Trace("domain.readUserHandler: error = " + service.UserUsernameNotFoundError)
			query.User.ErrorType = service.UserUsernameNotFoundError
			query.User.ErrorMessage = "User '" + query.User.Username + "' is not found"
		} else {
			log.Trace("domain.readUserHandler: found user")
		}
	}

	query.ReplyChan <- *query.User
}

// addUserHandler will add users to the persistent store
// NB it is assumed that admins can only add users
func (d *Domain) addUserHandler(ctx context.Context, query *types.UserOp, bypassSessionCheck bool) {
	log.Trace("domain.addUserHandler: starting")

	newEvent := service.EventUserAddError // assume it's failure

	// first check if the user being added is reserved
	// TODO: future enhancement will check a list of other reserved usernames
	if query.User.Username == service.ReservedCacaoSystemActor {
		log.Trace("domain.addUserHandler: error = " + service.UserUsernameReservedCannotAddError)
		query.User.ErrorType = service.UserUsernameReservedCannotAddError
		query.User.ErrorMessage = "User '" + query.User.Username + "' is a reserved username, cannot add"
		return
	} else {
		var authorized bool
		query.User.ErrorType, query.User.ErrorMessage, authorized, _ = d.getActorAuthorization(query.Op, query.User.SessionActor, query.User.Username)
		if authorized {
			err := d.UserStore.UserAdd(query.User)
			if err != nil { // for now, assuming cannot add
				if db.IsDuplicateError(err) {
					log.Trace("domain.addUserHandler: error = " + service.UserUsernameExistsCannotAddError)
					query.User.ErrorType = service.UserUsernameExistsCannotAddError
					query.User.ErrorMessage = "User '" + query.User.Username + "' already exists, cannot add"
				} else {
					log.Trace("domain.addUserHandler: error = " + service.UserUsernameNotFoundError)
					query.User.ErrorType = service.UserUsernameNotFoundError
					query.User.ErrorMessage = "User '" + query.User.Username + "' is not found"
				}
			} else {
				newEvent = service.EventUserAdded
			}
		}
	}

	err := d.EventsOut.PublishEvent(newEvent, *query.User)
	if err != nil {
		log.Warn("domain.addUserHandler: error received when publishing event, skipping for now: " + err.Error())
	}
}

// updateUserHandler updates the user in the persistent store
// NB users can update their own user (minus protected fields); otherwise, admins can update anyone and any field
func (d *Domain) updateUserHandler(ctx context.Context, query *types.UserOp, bypassSessionCheck bool) {
	log.Trace("domain.updateUserHandler: starting")

	newEvent := service.EventUserUpdateError // assume it's failure

	var authorized, isAdmin bool
	query.User.ErrorType, query.User.ErrorMessage, authorized, isAdmin = d.getActorAuthorization(query.Op, query.User.SessionActor, query.User.Username)

	if authorized {

		origUser := types.DomainUserModel{Username: query.User.Username}
		err := d.UserStore.UserGet(&origUser)
		if err == nil {

			// if regular user, we need to copy over the protected fields
			if !isAdmin {
				query.User.IsAdmin = origUser.IsAdmin       // admin state can only be changed by admins
				query.User.DisabledAt = origUser.DisabledAt // admin users can only change disabledAts
			}

			// we always protect these fields
			query.User.CreatedAt = origUser.CreatedAt

			err = d.UserStore.UserUpdate(query.User)
			if err != nil { // for now, assuming cannot add
				log.Trace("domain.updateUserHandler: error = " + err.Error())
				query.User.ErrorType = service.UserUpdateError
				query.User.ErrorMessage = "User '" + query.User.Username + "' update failed: " + err.Error()
			} else {
				newEvent = service.EventUserUpdated
			}
		} else { // this is bad since we should have been able to get the original user
			log.Trace("domain.updateUserHandler: error = " + err.Error())
			query.User.ErrorType = service.UserUpdateError
			query.User.ErrorMessage = "User '" + query.User.Username + "' update failed: " + err.Error()

		}
	}

	err := d.EventsOut.PublishEvent(newEvent, *query.User)
	if err != nil {
		log.Warn("domain.updateUserHandler: error received when publishing event, skipping for now: " + err.Error())
	}

}

func (d *Domain) deleteUserHandler(ctx context.Context, query *types.UserOp, bypassSessionCheck bool) {
	log.Trace("domain.deleteUserHandler: starting")

	newEvent := service.EventUserDeleteError // assume it's failure

	// Users can always get their own info; otherwise, must be admin
	var authorized, isAdmin bool
	query.User.ErrorType, query.User.ErrorMessage, authorized, isAdmin = d.getActorAuthorization(query.Op, query.User.SessionActor, query.User.Username)
	if authorized && isAdmin { // authorized = isAdmin, but good to double check
		err := d.UserStore.UserDelete(query.User.Username)
		if err != nil { // for now, assuming cannot add
			log.Trace("domain.deleteUserHandler: error = " + err.Error())
			query.User.ErrorType = service.UserDeleteError
			query.User.ErrorMessage = "User '" + query.User.Username + "' delete failed: " + err.Error()
		} else {
			newEvent = service.EventUserDeleted
		}
	}

	err := d.EventsOut.PublishEvent(newEvent, *query.User)
	if err != nil {
		log.Warn("domain.deleteUserHandler: error received when publishing event, skipping for now: " + err.Error())
	}

}

func (d *Domain) readUserListHandler(ctx context.Context, query *types.UserListQuery, bypassSessionCheck bool) {
	log.Debug("domain.readUserListHandler starting")

	// Users can always get their own info; otherwise, must be admin
	var authorized bool
	var results []types.DomainUserModel
	query.UserList.ErrorType, query.UserList.ErrorMessage, authorized, _ = d.getActorAuthorization(query.Op, query.UserList.SessionActor, "")
	if authorized { // for now, let's assume authorized = admin, but this will change later
		var err error
		results, err = d.UserStore.UserList(query.UserList.Filter)
		if err != nil { // for now, assuming not found
			log.Trace("domain.readUserHandler: error = " + service.UserUsernameNotFoundError)
			query.UserList.ErrorType = err.Error()
			query.UserList.ErrorMessage = err.Error()
		} else {
		}
	}

	query.ReplyChan <- results

	// TODO: EJS, need to add a switch statement and detetct context cancellation
	// total_slice_size := 10
	// log.Trace("QueryAdapter.processUserListQuery: total slize size = " + strconv.Itoa(total_slice_size))
	// test_data := make([]service.User, total_slice_size)
	// for i := 0; i < total_slice_size; i++ {
	// 	stringidx := strconv.Itoa(i)
	// 	newuser := service.UserModel{Username: "username" + stringidx, FirstName: "fn" + stringidx,
	// 		LastName: "ln" + stringidx, PrimaryEmail: "email" + stringidx + "@test.com"}
	// 	test_data[i] = &newuser
	// }
	// log.Trace("QueryAdapter.processUserListQuery: done creating demo data")

	// true_size := total_slice_size // assume all items
	// resp.NextStart = -1           // and there are no other items in the list
	// if total_slice_size > listquery.UserList.Filter.MaxItems {
	// 	if (listquery.UserList.Filter.Index + listquery.UserList.Filter.MaxItems) > total_slice_size {
	// 		true_size = total_slice_size - listquery.UserList.Filter.Index
	// 	} else {
	// 		true_size = listquery.UserList.Filter.MaxItems
	// 		resp.NextStart = listquery.UserList.Filter.Index + true_size
	// 	}
	// }

}

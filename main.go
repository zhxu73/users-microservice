package main

import (
	"context"
	"os"
	"os/signal"
	"users-microservice/adapters"
	"users-microservice/constants"
	"users-microservice/domain"

	log "github.com/sirupsen/logrus"

	"github.com/kelseyhightower/envconfig"
)

func main() {

	log.SetReportCaller(false)

	var s constants.Specification
	err := envconfig.Process(constants.ConfigVarPrefix, &s)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Debug("main(): setting a cancelable context")
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	s.AppContext = ctx // the ctx is passed along in the specification to be used in adapters

	// set the debug level
	loglvl, _ := log.ParseLevel(s.LogLevel)
	log.SetLevel(loglvl)

	log.Debug("specification = ", s)
	log.Debug("log level = ", loglvl)
	log.Debug("db url = ", s.DbURI)
	log.Debug("db database = ", s.DbName)
	log.Debug("queries nats url = ", s.NatsURL)

	// create an initial Domain object
	ea := adapters.EventAdapter{}
	qa := adapters.QueryAdapter{}
	var dmain *domain.Domain = domain.NewDomain(
		&qa, &ea, &ea, &adapters.MongoAdapter{},
	)

	// initialize the domain and all the adapters in the Init method
	// start the domain, passing the context
	dmain.Init(s)
	dmain.Start(ctx)

	// log.Printf("Waiting for messages, press ctrl+c to exit")
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}

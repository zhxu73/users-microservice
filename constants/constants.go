package constants

import (
	"context"
	"time"

	"github.com/nats-io/nats.go"
)

// Specification - holds the configurable constants for the microservice
type Specification struct {
	DbURI      string          `default:"mongodb://localhost:27017" split_words:"true"`
	DbName     string          `default:"users" split_words:"true"`
	ClusterID  string          `default:"cacao-cluster" split_words:"true"`
	NatsURL    string          `default:"nats://nats:4222" split_words:"true"`
	AppContext context.Context `ignored:"true"`
	LogLevel   string          `default:"debug" split_words:"true"`
}

// TODO: DEVELOPER should changes these e.g. instead of "mythical*", change to "user*" for user microservice, "credentials*", "workspaces*", etc
const (
	//DefaultNatsURL           = "nats://nats:4222"
	DefaultNatsMaxReconnect  = 6                // max times to reconnect within nats.connect()
	DefaultNatsReconnectWait = 10 * time.Second // seconds to wait within nats.connect()

	DefaultNatsClientID      = "users-microservice"
	DefaultNatsQGroup        = "users_qgroup"
	DefaultNatsClusterID     = "cacao-cluster"
	DefaultNatsDurableName   = "users_durable"
	DefaultNatsEventsSubject = "cyverse.events"

	DefaultNatsCoreSubject = ">"                        // this listens to all
	DefaultNatsCoreTimeout = 24 * 60 * 60 * time.Second // timeout before wiretap exipires, currently set so that 1 day is a-ok
	//DefaultNatsCoreTimeout = 30 * time.Second // timeout before wiretap exipires, currently set so that 1 day is a-ok
)

// ClientID - client id for nats streaming
const ClientID string = "users-microservice"

// ClientIDResponderExt is added to end of client id when responding on the events queue
const ClientIDResponderExt string = "-responder"

// EventsSubject - nats streaming queue subject for events
const EventsSubject string = "cyverse.events"

// EventsURLDefault - default nats streaming URL for events, used when EventsURL is not specified in Specification
const EventsURLDefault string = nats.DefaultURL

// QueriesURLDefault - default nats streaming URL for queries, used when QueriesURL is not specified in Specification
const QueriesURLDefault string = nats.DefaultURL

// UsersGetPropertySubject - subject for nats subscription for UsersGetProperty requests
const UsersGetPropertySubject string = "cyverse.users.getProperty"

// DefaultChannelBufferSize is the default size for channels
const DefaultChannelBufferSize = 100

// ConfigVarPrefix is the prefix used for environment variables
const ConfigVarPrefix string = "usersms"

// CloudEventSourceBaseValue is the source string value used for all outgoing cloudevent messages
const CloudEventSourceBaseValue string = "https://gitlab.com/cyverse/users-microservice/"

// EnvPodNameVar is used for determining unique source value
const EnvPodNameVar string = "POD_NAME"

// MongoDbUserCollectionName name of the mongo collection
const MongoDbUserCollectionName = "users"

// MaxUserListSize is the maximum number of users to return in a list
// TODO: ultimately, change this to a reasonably larger number
const MaxUserListSize = 500 // small for now
